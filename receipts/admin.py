from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.

class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass

class AccountAdmin(admin.ModelAdmin):
    pass

class ReceiptAdmin(admin.ModelAdmin):
    pass

admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)
