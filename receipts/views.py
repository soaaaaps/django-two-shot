from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from receipts.models import Receipt, ExpenseCategory, Account
from pprint import pprint

# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    paginate_by = 10

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/expenselist.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accountlist.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

class ReceiptCreateView(LoginRequiredMixin,CreateView):
    model = Receipt
    template_name = "receipts/new.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self,form):
        form.instance.purchaser=self.request.user
        return super().form_valid(form)

class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/newexpense.html"
    fields = ["name"]
    success_url = reverse_lazy("list_categories")

    def form_valid(self,form):
        form.instance.owner=self.request.user
        return super().form_valid(form)

class AccountCreateView(CreateView):
    model = Account
    template_name = "receipts/newaccount.html"
    fields = ["name", "number"]
    success_url = reverse_lazy("account_list_view")

    def form_valid(self,form):
        form.instance.owner=self.request.user
        return super().form_valid(form)
